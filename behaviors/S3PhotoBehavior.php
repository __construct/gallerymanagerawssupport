<?php

/**
 * This Behavior prepares GalleryPhoto model interaction with Amazon S3 service 
 */
class S3PhotoBehavior extends CActiveRecordBehavior
{
    /**
     * @see $this->owner::rules()
     * @return array
     */
    public function rules()
    {
        $rules = $this->owner->rules();
        // add rules for new fields
        $newRules = array(
            array('timeuploaded, timemodified', 'numerical', 'integerOnly'=>true),
        );
        return CMap::mergeArray($rules, $newRules);
    }
    
    /**
     *
     * @return null
     */
    public function initFileHandler()
    {
        $handler = GmS3Photo::getInstanceByPhoto($this);
        $this->owner->setFileHandler($handler);
    }
}