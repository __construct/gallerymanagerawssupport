<?php

/**
 * This Behavior prepares GalleryController interaction with Amazon S3 service
 */
class S3GalleryControllerBehavior extends CBehavior
{
    public function init()
    {
        $this->owner->init();
        Yii::import('application.extensions.galleryManager.behaviors.S3PhotoBehavior');
    }
    /**
     * @param int $id
     * @return null
     */
    public function loadGalleryPhoto($id)
    {
        $photo = parent::loadGalleryPhoto($id);
        $photo->attachBehavior('S3PhotoBehavior',array('class' => 'S3PhotoBehavior'));
        $photo->initFileHandler();
        
        return $photo;
    }
    
    /**
     * @param string $name
     * @return GmS3Photo
     */
    protected function loadImageHandler($name)
    {
        return GmS3Photo::getInstanceByName($name);
    }
}