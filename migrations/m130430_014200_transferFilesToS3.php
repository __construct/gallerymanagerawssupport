<?php
/**
 * Move all images to amazon S3 server
 * (need to do this with separate migration)
 *
 * @author Smirnov Ilia <frost@easycast.ru>
 */
class m130430_014200_transferFilesToS3 extends CDbMigration
{
    public function safeUp()
    {
        // disable time limits
        ini_set("max_execution_time","300000");
    
        Yii::import('application.extensions.galleryManager.models.*');
        Yii::import('application.extensions.galleryManager.*');
    
        if ( ! GalleryController::USE_AMAZON_S3 )
        {
            echo 'Amazon storage disabled. Migration canceled.';
            return;
        }
        
        $galleries = GalleryS3::model()->findAll();
        
        foreach ( $galleries as $gallery )
        {
            echo 'Processing gallery '.$gallery->id." ...\n";
            if ( $photos = $gallery->notUploadedPhotos )
            {
                foreach ( $photos as $photo )
                {
                    echo 'Uploading photo '.$photo->id."\n";
                    try
                 {// trying to upload the photo
                        $photo->setImageS3();
                    } catch ( Exception $e )
                    {
                        echo 'Timeout. Another try...'."\n";
                        // need to comment this part.
                        // Sometimes, amazon server suddenly close socket connection by timeout.
                        // It happens in a very few cases by we shoud keep it in mind.
                        // In this case we just restart the upload process
                        try
                     {
                            $photo->setImageS3();
                        }catch ( Exception $e )
                        {// second error on same file shoud never happen
                            // cron will take care about skipped photos later anyway
                            echo 'Failed. Move to next photo. Image Skipped. '."\n";
                        }
                    }
        
                    // update upload time
                    $photo->save();
                    unset($photo);
                    // avoiding SlowDown errors
                    sleep(1);
                }
                // need to sleep for a while.
                // S3 closes connection if you are trying to upload too many files too fast
                // We need to upload images by small portions
                // more information here: http://docs.aws.amazon.com/AmazonS3/latest/dev/ErrorBestPractices.html
                echo('Sleeping 30 seconds...');
                sleep(30);
            }
        }
    }
}