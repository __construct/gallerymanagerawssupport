<?php

/**
 * Prepare tables to move all images to amazon S3 server
 * 
 * @author Smirnov Ilia <frost@easycast.ru>
 */
class m130427_173000_addUploadTime extends CDbMigration
{
    public function safeUp()
    {
        // disable time limits
        ini_set("max_execution_time","300000");
        
        Yii::import('application.extensions.galleryManager.models.*');
        Yii::import('application.extensions.galleryManager.*');
        
        if ( ! GalleryController::USE_AMAZON_S3 )
        {
            echo 'Amazon storage disabled. Migration canceled.';
            return;
        }
        
        $table = "{{gallery_photo}}";
        $this->addColumn($table, 'timemodified', "int(11) UNSIGNED NOT NULL DEFAULT 0");
        $this->createIndex('idx_timemodified', $table, 'timemodified');
        $this->addColumn($table, 'timeuploaded', "int(11) UNSIGNED NOT NULL DEFAULT 0");
        $this->createIndex('idx_timeuploaded', $table, 'timeuploaded');
        
    }
}