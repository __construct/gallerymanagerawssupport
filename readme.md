Amazon Web Services support for Gallery Manager plugin: http://www.yiiframework.com/extension/gallerymanager/

This plugin adds file upload to Amazon S3 bucket for all galley photos.

Requirements: Yii Amazon AWS API required for this extension: http://www.yiiframework.com/extension/yii-aws

IMPOTANT: for now, this plugin uses my fork from original galleryManager plugin: https://bitbucket.org/__construct/gallerymanager
We plan to merge this later.