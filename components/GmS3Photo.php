<?php

/**
 * Class for upload GalleryPhoto image to Amazon S3 storage
 */
class GmS3Photo extends GmUploadedPhoto
{
    /**
     * @var string
     */
    public static $newInstanceClass = 'GmS3Photo';
    /**
     * @var bool - upload images to Amazon S3 Storage instantly after save?
     */
    const AUTO_UPLOAD_IMAGES = true;
    /**
     * @var A2S3 - AWS API object
     */
    protected static $_s3;
    /**
     * @var string - amazon S3 bucket name
     */
    protected static $_bucket;
    /**
     * @var string - base url of images bucket
     */
    protected static $_bucketBaseUrl;
    /**
     * @var bool - flag to mark image uploaded
     */
    protected static $_imagesUploaded = false;
    /**
     * @var bool - flag to mark image modified
     */
    protected static $_imagesModified = false;
    
    /**
     * (non-PHPdoc)
     * @see GmUploadedPhoto::loadData()
     */
    public function loadData($photo)
    {
        parent::loadData($photo);
        $this->initS3();
    }
    
    /**
     * (non-PHPdoc)
     * @see GmUploadedPhoto::loadData()
     */
    protected static function preparePhoto($photo)
    {
        Yii::import('application.extensions.galleryManager.behaviors.S3PhotoBehavior');
        $photo = parent::preparePhoto($photo);
        $photo->attachBehavior('S3PhotoBehavior',array('class' => 'S3PhotoBehavior'));
        $photo->initFileHandler();
        
        return $photo;
    }
    
    /**
     * Set up all params for Amazon S3 storage
     */
    protected function initS3()
    {
        self::$_s3             = new A2S3(Yii::app()->params['AmazonS3Config']);
        self::$_bucket         = Yii::app()->params['AWSBucket'];
        self::$_bucketBaseUrl  = Yii::app()->params['AWSBucketPath'];
    }
    
    /**
     * 
     * @param GalleyPhoto $photo
     * @return bool
     */
    public static function isUploaded($photo)
    {
        $photo = static::preparePhoto($photo);
        if ( ( $photo->timemodified > $photo->timeuploaded ) OR ! $photo->timeuploaded )
        {
            return false;
        }
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see GmUploadedPhoto::setImage()
     */
    public static function setImage($photo, $path, $setSystem=true)
    {
        $photo = static::preparePhoto($photo);
        parent::setImage($photo, $path, $setSystem);
        $photo->timemodified = time();
        $photo->save();
        
        if ( self::AUTO_UPLOAD_IMAGES )
        {
            self::setImageS3($photo, $setSystem);
        }
        return true;
    }
    
    /**
     * Upload all image versions to Amazon S3 bucket
     * @param GalleyPhoto $photo
     * @param bool $includeSystem - upload system(original) image?
     * @return bool
     */
    public static function setImageS3($photo, $includeSystem=true)
    {
        $photo    = static::preparePhoto($photo);
        $versions = self::createVersionsList($photo->gallery, $includeSystem);
        foreach ( $versions as $version=>$actions )
        {
            $source = self::getSavePath($photo, $version);
            $target = parent::getUrl($photo, $version, false);
            try
            {// trying to upload image...
                self::uploadImageToS3($source, $target);
            }catch ( Exception $e )
            {// upload failed - no problem: skip it and try later by cron
                // @todo log this
                return false;
            }
        }
        // all done, mark photo as uploaded
        $photo->timeuploaded = time();
        return $photo->save();
    }
    
    /**
     * Upload one image version to S3 Bucket
     * @param string $source - full path to file
     * @param string $target - destination
     * @return bool
     * @throws Exception
     */
    protected static function uploadImageToS3($source, $target)
    {
        if ( ! file_exists($source) )
        {// no source file
            return false;
        }
        $request = array();
        $request['Bucket']     = self::$_bucket;
        $request['ACL']        = 'public-read';
        $request['Key']        = $target;
        $request['SourceFile'] = $source;
        // put image into bucket
        // @todo check image upload result
        $result = self::$_s3->putObject($request);
        // We can poll the object until it is accessible
        // @todo "waitUntilObjectExists" doesn't work (non existing function(???)) waiting for API updates
        /*self::$_s3->waitUntilObjectExists(
         array(
             'Bucket' => $this->_bucket,
             'Key'    => $target,
         )
        );*/
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see GmUploadedPhoto::removeImages()
     */
    public static function removeImages($photo, $includeSystem=true)
    {
        self::removeImagesFromS3($photo, $includeSystem);
        return parent::removeImages($photo, $includeSystem);
    }
    
    /**
     * Delete image versions from from amazon S3 bucket
     * @param GalleyPhoto $photo
     * @param bool $includeSystem - upload system(original) image?
     * @return bool
     * @throws Exception
     */
    public static function removeImagesFromS3($photo, $includeSystem=true)
    {
        $photo    = static::preparePhoto($photo);
        $versions = self::createVersionsList($photo->gallery, $includeSystem);
        $objects  = array();
        if ( ! $photo->timeuploaded )
        {// images never uploaded - nothing to delete
            return true;
        }
        foreach ($versions as $version=>$actions)
        {// collect objects to delete
            $objects[] = array(
                'Key' => parent::getUrl($photo, $version, false),
            );
        }
        // set request params
        $request = array();
        $request['Bucket']  = self::$_bucket;
        $request['Objects'] = $objects;
        try
        {// send request to Amazon API
            self::$_s3->deleteObjects($request);
        }catch ( Exception $e )
        {// image not deleted
            // @todo log this
            return false;
        }
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see CUploadedFile::getUrl()
     */
    public static function getUrl($photo, $version='', $absolute=true)
    {
        if ( self::isUploaded($photo) )
        {
            return self::$_bucketBaseUrl.'/'.parent::getUrl($photo, $version, false);
        }else
        {
            return parent::getUrl($photo, $version, $absolute);
        }
    }
}